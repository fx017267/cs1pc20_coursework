cd portfolio
: Switches to portfolio directory

git switch master
: Switches to master branch

mkdir -p week4/framework
cd week4/framework
: Make directory called week4 and switch to it

git branch framework
git switch framework
: Create branch called framework and switch to it

cat -vTE Makefile
: Prints contents of makefile, displaying:
 - "$" at the end of each line
 -  Tab characters as "^I".

make feature NAME=test_output
: Executes the bash commands under "feature" in the makefile, taking variable NAME as "test_output"

ls -al test_output
: Shows all files in directory test_output.

git add Makefile
git commit -m "Setting up Makefile to create feature folders"
git push
: Stages, commits and pushes makefile. Shows an error as current git branch framework has no upstream. Will wait until end of exercise to merge framework back into local master branch, then push from there.

cd test_output; cd src
: Switches to test_output directory, then to test_output/src. Throws an error as test_output/src does not exist, so I manually created that directory.

gcc -Wall -pedantic test_outputs.c -o test_outputs
: Compiles test_outputs.c into executable test_outputs

./test_outputs file_does_not_exist
: Runs test_outputs with file_does_not_exist as an argument. Fails because, predictably, file_does_not_exist does not exist. 

./test_outputs 
: Runs test_outputs. Fails because no file is passed as a parameter. 

./test_outputs op_test
: Runs test_outputs with newly created op_test as an argument. 

git add test_outputs.c
git add op_test
git commit -m "test framework and sample test suite"
git push
: Stages specified files, commits them then pushes to remote repo
