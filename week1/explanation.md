mkdir -p $HOME/portfolio/week1 ; cd $HOME/portfolio/week1 
: Makes a directory portfolio, along with a "subdirectory" week1 within it, then switches to that folder.

cd ~ 
: Goes home

rm -r portfolio
: Removes portfolio directory, the -r flag indicates that it recursively deletes directories within it.

mkdir -p $HOME/portfolio/week1 & cd $HOME/portfolio/week1
: Makes a directory portfolio, along with a "subdirectory" week1 within it, and then executes a command that should switch to it in the background, but does not as the directory is not yet created by the time the latter command is executed.

cd ~
: Goes home

rm -r portfolio
: Removes portfolio directory, the -r flag indicates that it recursively deletes directories within it.

mkdir -p $HOME/portfolio/week1 && cd $HOME/portfolio/week1
: Makes a directory portfolio, along with a "subdirectory" week1 within it, and then, if the command executes without error, switches to it.

echo "Hello World"
: Prints "Hello World" to screen

echo Hello, World
: Prints "Hello, World" to screen

echo Hello, world; Foo bar 11. $ echo Hello, world!
: Prints "Hello, World" to screen, but errors out after realizing "Foo" is not a valid command

echo "line one";echo "line two"
: Prints "line one", then "line two" to screen

echo "Hello, world > readme"
: Prints "Hello, world > readme" to screen

echo "Hello, world" > readme
: Puts output text of echo command into file called "readme"

cat readme
: Prints contents of readme to screen

example="Hello, World"
: Sets variable "example" to "Hello, World"

echo $example
: prints contents of variable "example" to screen

echo ’$example’
: prints contents of variable "example" to screen, inside the apostrophes

echo "$example"
: prints contents of variable "example" to screen

echo "Please enter your name."; read example
: prints "please enter your name" to screen, takes a name as input, which is put into variable example

echo "Hello $example"
: prints contents of variable "example" to screen, preceded by "hello"

three=1+1+1;echo $three
: sets variable three to string 1+1+1, then prints value of three to screen, which is not 3.

bc
: Couldn't find command "bc"

echo 1+1+1 | bc
: Couldn't find command "bc"

let three=1+1+1;echo $three
: sets variable three to integer evaluation of 1+1+1, then prints value of three to screen, which is 3.

echo date
: prints the word "date"

cal
: Shows a calendar with today's date highlighted

which cal
: returns filesystem location of command cal

/bin/cal
: Executes command cal

$(which cal)
: No result

‘which cal‘
: Couldn't find commant 'which cal'. Anything in Apostrophes sems to be treated as 1 token.

echo "The date is $(date)"
: Prints the date in place of "$(date)"

seq 0 9
: Prints numbers 0-9 in sequence

seq 0 9 | wc -l
: Prints number of lines in that sequence, which was 10

seq 0 9 > sequence
: Puts the output of "Seq 0 9" into a newly created file called sequence

wc -l < sequence
: Gets no. of lines in file "sequence" and prints it.

for I in $(seq 1 9) ; do echo $I ; done
: Iterates through numbers 1 to 9, printing the current number at each iteration.

(echo -n 0 ; for I in $(seq 1 9) ; do echo -n +$I ; done ; echo) | bc
: Couldn't find command "bc"

echo -e ‘#include <stdio.h>\nint main(void) \n{\n printf(“Hello World\\n”);\n return 0;\n}’ > hello.c
: Puts code for a C hello world program into

cat hello.c
: Prints contents of "hello.c"

gcc hello.c -o hello
: Compiles hello.c into an executable called hello

./hello
: Runs executable hello