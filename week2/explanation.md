cd ~ 
: goes home

git init portfolio 
: creates git repo in directory portfolio

cd portfolio
: switches to portfolio directory 

ls -al 
: Shows all files in working directory, along with their creator and creation datetime

git status 
: Shows general git info

echo hello > .gitignore 
: Writes "hello" to new file called .gitignore

git add -A 
: Stages all changed files

git status 
: Now shows .gitignore under "changes to be committed"

git config --global user.email “<student_email>” 
git config –global user.name “<student_name>” 
: Establishing credentials 

git commit -m “first commit, adding week 1 content” 
: Commit staged changes

git status
: Shows general git info - reflects that no changes are staged as a result of commit

git push 
: Attempts to push changes to a remote repo, fails because none has been set yet

git remote add origin https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio.git 
: adds gitlab repo as remote repo

git push –set-upstream origin main
: Sets as upstream so no need to specify branch & repo to push to in later pushes

git status
: Shows general git info - now reflects changed branch and week2 folder as untracked

echo “# CS1PC20 Portfolio” > readme.md 
: Puts “# CS1PC20 Portfolio” into new file called readme.md

git add readme.md 
: Stages readme.md

git commit -m “added readme file” 
: Commits changes

git push 
: Pushes commited changes to remote gitlab repo

git config –global credential.helper cache 
: Saves credentials for future git usage

git branch week2 
: Create new branch called week2

git checkout week2 
: Switch to branch week2

mkdir week2 
: Create directory week2

echo “# Week 2 exercise observations” > week2/report.md 
: Puts “# Week 2 exercise observations” into new file called readme.md

git status
: Shows general git info - now reflects changed branch and week2 folder as untracked

git add week2
: Stage all changes in directory week2

git commit -m “added week 2 folder and report.md” 
: Commit staged changes

git push 
: Push commit to remote

git checkout master 
: Switch to master branch

ls -al 
: Shows all files in working directory, along with their creator and creation datetime

git checkout week2 
: Switches to branch week2

ls -al 
: Shows all files in working directory, along with their creator and creation datetime

git checkout master 
: Switches to branch master

git merge week2 
: Implements changes in week2 branch in main branch

ls -al 
: Shows all files in working directory, along with their creator and creation datetime

git push 
: Pushes changes to main branch to remote repo

rm -r week2 
rm -r week1 
Recursively deletes folders week1 and week2

ls -al 
: Shows all files in working directory, along with their creator and creation datetime

git status 
: Shows general git info

git stash
: Shelves uncommitted changes, reverting the state of the repo to it's last commit 

git stash drop 
: Deletes the stash

ls -al
: Shows all files in working directory, along with their creator and creation datetime

cd ~ 
: Goes home

cp -r portfolio portfolio_backup 
Copies all of portfolio directory into new directory portfolio_backup

rm -rf portfolio 
: Recursively deletes whole portfolio

ls -al 
: Shows all files in working directory, along with their creator and creation datetime

git clone https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio portfolio 
: Creates a new repo, which is a copy of the remote repo at the gitlab URL

ls -al 
: Shows all files in working directory, along with their creator and creation datetime