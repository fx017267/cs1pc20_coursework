cd ~
: Changes working directory to $HOME

cd portfolio
: Changes working directory to portfolio

mkdir week3
: Create directory week3

mkdir week3/greeting
: Create directory greeting within week3

cd week3/greeting
: Changes to the aforementioned directory

git branch greeting
: Create git branch called greeting

git switch greeting
: Switch to git branch greeting

gcc -Wall -pedantic -c greeting.c -o greeting.o
: Calls the GCC, which compiles c file greeting.c into executable file greeting.o. The option -pedantic makes the compiler more likely to generate warnings.

echo greeting.o >> ~/portfolio/.gitignore
: puts text "greeting.o" into .gitignore file in the portfolio directory. .gitignore is created by this command as it doesn't already exist.

echo libgreet.a >> ~/portfolio/.gitignore
: puts text "libgreet.a" into .gitignore file in the portfolio directory. 

ar rv libgreet.a greeting.o
:Creates an archive file of greeting.o caled libgreet.a

gcc test_result.c -o test1 -L. -lgreet -I.
: Compiles test_result.c into machine code in executable file test1

./test1
: Runs executable file test1. test1 uses the function greet() mentioned in the header file it includes (greeting.h) to print "Hello World". The assert() function prevents the program from continuing if the function greet() returns an error.

git add -A
: Stages all changes

git commit -m “greeting library and greeting test program”
: Commits changes

git push
: Pushes changes to remote repo

git switch master
: Switches working branch to the master branch

git branch vectors
: Creates new branch called vectors

git switch vectors
: Switches current branch to vectors

cd ~/portfolio/week3
: Changes working directory to week3 in portfolio

mkdir vectors
: Creates folder called vectors

cd vectors
: Moves working directory to that folder

gcc -Wall -pedantic -c vector.c -o vector.o
: Compiles vector.c into executable file vector.o

ar rv libvector.a vector.o
: Creates archive file libvector.a from vector.o executable

gcc test_vector_add.c -o test_vector_add1 -L. -lvector -I.
: compiles test_vector_add.c into machine code and puts the result into executable test_vector_add1

./test_vector_add1
: Runs executable test_vector_add1

git add -A
: Stages all changes

git commit -m “code to add two vectors of fixed size”
: Commits all changes

git push
: Should push all changes, but doesn't, since vector branch has no upstream, unlike master.

36. Change the assert(5==zvec[2]); line to be assert(5==zvec[1]); and recompile test to see what happens. - This does not give an error at compile time, but aborts at run time as the condition specified in assert(5==zvec[1]) is not satisfied

gcc -Wall -pedantic -c vector.c -o vector.o
: Attempts to recompiles vector.c into machine code in executable vector.o - However an error is returned due to 2 conflicting definitions of the dot_product function (in vector.c and vector.h) - one takes 2 params and another takes 3. This was fixed by deleting the 3rd param of the latter

ar rv libvector.a vector.o
: Creates library file libvector.a from vector.o executable

gcc test_vector_dot_product.c -o test_vector_dot_product1 -L. -lvector - I.
: Compiles test_vector_dot_product.c into executable test_vector_dot_product1

./test_vector_dot_product1
: Runs executable test_vector_dot_product1

git add -A
: Stage all changes

git commit -m “code to calculate dot product of two vectors of fixed size”
: Commit

git push
: Should push all changes, but doesn't, since vector branch has no upstream, unlike master. Solved this by using git push origin vectors, specifying the branch to push from. This created a new "vectors" branch in the remote repo, which was merged with the main branch (Ideally I should have merged the branch vectors with the main branch on my local repo, then pushed to remote)

