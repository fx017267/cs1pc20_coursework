#include <assert.h>
#include "vector.h"
/** A simple test framework for vector library
* we will be improving on this later
*/
int main(void) {
/** xvec and yvec will be inputs to our vector arithmetic routines
Figure 1 Used to say this - why would this be wrong?
CS1PC20, Exercise – Week 3 4/6
28. And now create the code to actually “do the math” – vector.c
Like we did with the greeting code, we will compile to a library, and then use that library when we compile the
test program:
29. $ gcc -Wall -pedantic -c vector.c -o vector.o
30. $ ar rv libvector.a vector.o
31. $ gcc test_vector_add.c -o test_vector_add1 -L. -lvector -I.
32. $ ./test_vector_add1
33. $ git add -A
34. $ git commit -m “code to add two vectors of fixed size”
35. $ git push
Well – that isn’t very clear, but if it says nothing, it worked!
36. Change the assert(5==zvec[2]); line to be assert(5==zvec[1]); and recompile test to see what happens.
Now let us add a function to calculate the “dot product” of two fixed size vectors.
37. Edit vector.h so it contains this:
* zvec will take the return value
*/
 int xvec[SIZ]={1,2,3};
 int yvec[SIZ]={5,0,2};
 int zvec[SIZ];
 add_vectors(xvec,yvec,zvec);
 /** We want to check each element of the returned vector
 */
 assert(6==zvec[0]);
 assert(2==zvec[1]);
 assert(5==zvec[1]);
 /** If the asserts worked, there wasn’t an error so return 0
 */
 return 0;
}