#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define NUM_LINES 10
#define NUM_CONNECTIONS 3


// Represents a room.
typedef struct connection connection; // Forward declaration for type connection, which is mentioned in room struct
typedef struct room {
  char* name;
  char* description;
  connection *connections;
} room;


// Represents a connection between 2 rooms
typedef struct connection {
  room* room1;
  room* room2;
} connection;


// Represents a weapon
typedef struct weapon {
    char* name;
    int maxDmg;
    int minDmg;
} weapon;


// Represents the player
typedef struct player {
  room* currentRoom;
  int health;
  weapon* weapons;
} player;


// Represents an instance of the game
typedef struct game {
  room* rooms;
  int numRooms;
  player* player;
} game;


// Represents an enemy
typedef struct enemy {
    char* name;
    char* description;
    int maxDmg;
    int minDmg;
    int health;
} enemy;


// Prints a menu, then gets & returns user choice
int getMenuChoice() {

  int choice;
  printf("What would you like to do?\n");
  printf("1. Look around\n");
  printf("2. Move to another room\n");
  printf("3. Fight an enemy\n");
  printf("4. Quit\n");
  scanf("%d", &choice);

  // Check that choice is valid
  while (choice < 1 || choice > 4) {
    printf("Invalid choice, please try again\n");
    scanf("%d", &choice);
  }
  return choice;
}


// Print the room description
void printRoomDescription(room* room) {
  printf("You are in the %s.\n", room->name);
  printf("%s\n", room->description);
}


// Get user choice of room to move to
int getRoomChoice(room* currentRoom) {

  int choice;
  printf("Which room would you like to move to?\n");
  for (int i = 0; i < 3; i++) {
    printf("%d. %s\n", i+1, currentRoom->connections[i].room2->name);
  }
  scanf("%d", &choice);

  // Check that choice is valid
  while (choice < 1 || choice > 3) {
    printf("Invalid choice, please try again\n");
    scanf("%d", &choice);
  }

  return choice;
}


// Moves player to another room & prints description of destination
void movePlayer(player* player, int choice) {
  player->currentRoom = player->currentRoom->connections[choice-1].room2;
  printRoomDescription(player->currentRoom);
}


// Loads rooms from rooms.csv file & returns an array of rooms
room* loadRooms() {

  FILE* file = fopen("rooms.csv", "r");
  if (file == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  room* rooms = malloc(sizeof(room) * NUM_LINES);
  char line[500];

  // Read room names & description from file
  for (int i = 0; i < NUM_LINES; i++) {

    fgets(line, 500, file); 
    /*
    we need to split the line into the name and description
    the strings are in quotation marks, and the two quoted strings are separated by a comma
    we need to split the line on ", " (the four characters)
    everything between the first and second " is the name
    everything between the third and fourth " is the description
    */

    char* name = strtok(line, "\"");
    char* comma=strtok(NULL, "\""); // The second token will be the comma interposed between the name and description
    char* description = strtok(NULL, "\"");

    // we need to create a room
    room room;
    //we need to copy the string into the room name
    room.name = malloc(sizeof(char)*(strlen(name) + 1));
    strcpy(room.name, name);
    //we need to copy the string into the room description
    room.description = malloc(sizeof(char)*(strlen(description) + 1));
    strcpy(room.description, description);
    room.connections = malloc(NUM_CONNECTIONS * sizeof(connection));
    
    rooms[i] = room;

  }

  fclose(file);

  FILE *connFile = fopen("connections.csv", "r");
  
  int connections[10][3]; // 2D array of connections

  char connString[7];
  for (int i=0; i < NUM_LINES; i++) {
    fgets(connString, 7, connFile);
    connString[6] = "\0"; // Replace newline character from file with null byte, 	
    int tokenizedConns[3];

    char *token = strtok(connString, ",");
    for (int j=0; j < NUM_CONNECTIONS; j++) {
        tokenizedConns[j] = atoi(token);
        token = strtok(NULL, ",");
    }
    memcpy(connections[i], tokenizedConns, sizeof(tokenizedConns));
  }

  fclose(connFile);

  for (int i = 0; i < NUM_LINES; i++) {
    for (int j = 0; j < NUM_CONNECTIONS; j++) {
      connection connection;
      connection.room1 = &rooms[i];
      connection.room2 = &rooms[connections[i][j]];

      rooms[i].connections[j] = connection;
    }
  }

  return rooms;
}

// Creates a player
player* createPlayer(room* currentRoom) {

  player* player = malloc(sizeof(player));
  player->currentRoom = currentRoom;
  player->health = 1000;
  
  player->weapons = malloc(4*sizeof(weapon)); 
  
  // Gettings player's weapons
  FILE* wpnsFile = fopen("weapons.csv","r");
  char line[500];
  for (int i = 0; i < 4; i++){
    weapon weapon;
    fgets(line, 500, wpnsFile);

    char* name = strtok(line, "\"");
    weapon.name = malloc(sizeof(char) * (strlen(name)+1));
    strcpy(weapon.name, name);

    weapon.maxDmg = atoi(strtok(NULL, ","));

    // The last token will contain at it's end the newline character at the end of the line. This removes that newline so the string can be converted to int.
    char *minDmg = strtok(NULL, ","); 
    int len = strlen(minDmg);
    minDmg[len - 1] = '\0'; // Replace newline char w/ null byte, effectively truncating the string.
    weapon.minDmg = atoi(minDmg);

    player->weapons[i] = weapon;
  }
  
  return player;
}

// Generates a random enemy to be fought
enemy getEnemy(){

  // Getting all possible enemies from file
  FILE* file = fopen("enemies.csv", "r");
  enemy* enemies = malloc(12*sizeof(enemy));
      
  char line[500]; 
  for (int i = 0; i < 12; i++){
    fgets(line, 500, file);
      
    enemy enemy;
    char* name = strtok(line, "\"");
    char* comma = strtok(NULL, "\""); // Second token will be the comma interposed between name and description
    char *description = strtok(NULL, "\"");
    
    enemy.name = malloc(sizeof(char)*(strlen(name)+1));
    enemy.description = malloc(sizeof(char)*(strlen(description)+1));
    strcpy(enemy.name, name);
    strcpy(enemy.description, description);

    // Numerical attributes are seperated solely by commas (Don't have quotemarks)
    enemy.maxDmg = atoi(strtok(NULL, ","));
    enemy.minDmg = atoi(strtok(NULL,","));

    // The last token will contain at it's end the newline character at the end of the line. This removes that newline so the string can be converted to int.
    char *health = strtok(NULL, ","); 
    int len = strlen(health);
    health[len - 1] = '\0'; // Replace newline char w/ null byte, effectively truncating the string.
    enemy.health = atoi(health); 

    enemies[i] = enemy;
  }

  // Choosing random enemy
  srand(time(NULL));
  int randIndex = (rand() % 12); // Randomly generate array index
  return enemies[randIndex];
}


// Represents a fight between the player and a given enemy.
void fight(player* player, enemy* enemy){
    printf("------------------------------------------------------------------------------------------------------\n");
    printf("You encounter a %s with %d health!:\n%s\n\n", enemy->name, enemy->health, enemy->description);
    
    srand(time(NULL));
    int enemyHlth = enemy->health; // This is so we don't directly modify the struct property, which is intended to be constant
    while ((player->health > 0) && (enemy->health > 0)){
        
        // Player's turn
        printf("YOUR TURN. Choose a weapon to attack with:\n");
        for (int i = 0; i < 4; i++){
            printf("%d. %s\n", i+1, player->weapons[i].name);    
        }
        int choice; 
        scanf("%d", &choice);
        // Validate user's choice
        while(choice < 1 || choice > 4){
            printf("Invalid choice, please try again\n");
            scanf("%d", &choice);
        }
        weapon weapon = player->weapons[choice-1];

        int dmg = (rand() % (weapon.maxDmg - weapon.minDmg + 1)) + weapon.minDmg;
        enemyHlth -= dmg;
        printf("You used: %s, which dealt %d damage\nThe %s now has: %d health\n\n", weapon.name, dmg, enemy->name, enemyHlth);
        
        if (enemyHlth <= 0){
            printf("The %s has been defeated!\n", enemy->name);
            printf("------------------------------------------------------------------------------------------------------\n");
            return;
        }
        
        // Enemy's turn
        printf("ENEMY'S TURN\n");
        int enemyDmg = (rand() % (enemy->maxDmg - enemy->minDmg + 1)) + enemy->minDmg;
        player->health -= enemyDmg;
        printf("%s dealt %d damage\nYou now have %d health\n\n", enemy->name, enemyDmg, player->health);

        if (player->health <= 0){
            printf("YOU HAVE DIED\n");
            exit(0); // Terminate program if user dies
        }
    }
}


// Creates a game instance
game* createGame() {
 
  game* game = malloc(sizeof(game));
  printf("debug - about to load rooms\n");
  game->rooms = loadRooms();
  printf("debug - rooms loaded\n");
  game->numRooms = NUM_LINES;
  game->player = createPlayer(&game->rooms[0]);
  return game;
}


// Driver code for playing the game
void playGame() {

  printf("Welcome to the game\n");
  game* game = createGame();
  printRoomDescription(game->player->currentRoom);
  // Loop until the user quits
  bool quit = false;
  while (!quit) {
    int choice = getMenuChoice();
    if (choice == 1) {
      printRoomDescription(game->player->currentRoom);
    } else if (choice == 2) {
      int choice = getRoomChoice(game->player->currentRoom);
      movePlayer(game->player, choice);
    } else if (choice == 3) {
      enemy enemy = getEnemy();
      fight(game->player, &enemy);
    } else if (choice == 4){
      quit = true;
    }
  }
}


int main() {
  playGame();
  return 0;
}










