# Example README.md showing required header and table format

| Module Code:                     |   CS1PC20                         |
|----------------------------------|-----------------------------------|
| Assignment report Title:         |  Assessment 1 portfolio           |
| Date (when the work completed):   |  25/11/23                        |
| Actual hrs spent for the assignment: | 8                              |


## Analysis of effect of CoPilot on Pat's coding

| Impact                   | Positive         | Negative    |
|--------------------------|------------------------|-----------------------|
| on Learning               | A learning programmer can use AI code generation to handle mundane aspects of programming, which allows them to focus on "interesting" areas where there are key concepts to be learnt | May foster dependence on AI for an inexperienced programmer, which may prevent the programmer from learning underlying concepts. Inexperienced programmers are also unlikely to be able to notice errors made by the code-generating AI
| on Productivity           | Can generate mock/fake data (As seen in room names). Can also generate code for commonly used processes e.g copying strings. Can generate struct fields based on comments & other provided info. Saving time & effort in all cases | It is possible for the AI to generate bad code/data that is likely to inconvenience the programmer|

## Analysis of effect of CoPilot on my programming for tasks 3 and 4

| Impact                   | Positive         | Negative    |
|--------------------------|------------------------|-----------------------|
| on Learning                | Good as a sort of substitute for stack overflow that can explain conventions & suitable approaches (e.g when to allocate heap memory, whether to use strcpy() or strncpy()), rather than simply providing a quick fix for a bug. It provides intuitive explanations that would be enormously beneficial for an inexperienced programmer. Can also explain to an inexperienced programmer the general idea behind parts of code that may beyond their understanding, allowing them to focus on aspects that are appropriate for their level while only getting "The Gist" of more difficult parts that, while beyond their skill level, are still necessary to understand the program | Found myself tempted to use it as a route to a quick fix for a bug, which can easily detract from the learning & understanding of the debugging process, one of the most vital elements of programming.  |
| on Productivity           | Was very effective when used to generate code for commonly-encountered tasks, which saved time. Data generation also saved time & effort that would have been spent thinking about the types of enemies to include in the game, rather than actually programming them.| Limitations re it's ability to properly understand context made it a poor debugging tool, in my experience. Beyond that it was very inconsistent in solving any task that needed an understanding of the context of the program and that was more complex than "Write code to do X given Y". |